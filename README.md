# bds-db-design 
                                **Project**             
                                **Database Design**         
_Maksim Avgustinopolskii(231226) 02.11.2021
Aidana Kurmanova(227247)

Popis databázy:
Naše databáze je databáze Hogwartsu(Brodavice) z vesmíru o knihách a filmech 
Harryho Pottera, uspokojí potřeby skladování, správy, šetření a získávání všech údajů 
o fantasy světě Harryho Pottera. V naší databázi najdete seznam učitelů a studentů 
vesmíru Harryho Pottera, jejich kontakty, vztahy mezi sebou. Můžete také najít 
studijní výsledky studentů Hogwartsu, osobní a obchodní schůzky studentů a učitelů. 
Můžete si vzít z naší databáze seznam fakult univerzity a jednotlivých školních 
předmětů. A v naší databázi je i školní personál a školní akce, které jsou tak slavné v 
tomto fantastickém vesmíru Harryho Pottera. 

Naše databáze byla vytvořena ve dvou databázových systémech-Postgresql a MySql.
V prvním případě jsme psali celý kód ručně, čerpající z grafu tabulky vytvořené dříve.
Ve druhém případě jsme vytvořili graf "Crow's Foot Notation" v MySQL 
Workbench a přeměnili jej pomocí" forward-engineering " na kód pro MySQL

GitLab project repository: https://gitlab.com/solitermxm/bds-db-design
